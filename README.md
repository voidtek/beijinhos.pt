# beijinhos.pt

This is the source code for [beijinhos.pt](http://beijinhos.pt). The site is fully static, powered by [Hugo](https://gohugo.io) and hosted on [Gitlab](https://gitlab.com/).

## Installation
Clone the repository and enter its directory